var LinkedinResource = require('../LinkedinResource');
var linkedinMethod = LinkedinResource.method;

module.exports = LinkedinResource.extend({

  path: 'people/~/shares',

  create: linkedinMethod({
    method: 'POST'
  })

});
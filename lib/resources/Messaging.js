var LinkedinResource = require('../LinkedinResource');
var linkedinMethod = LinkedinResource.method;

module.exports = LinkedinResource.extend({

  path: 'people/~/mailbox',

  create: linkedinMethod({
    method: 'POST'
  })

});
var LinkedinResource = require('../LinkedinResource');
var linkedinMethod = LinkedinResource.method;

module.exports = LinkedinResource.extend({

  path: 'people',

  retrieveOwn: linkedinMethod({
    method: 'GET',
    path: function(urlData) {
      var url = '~/connections';
      if (urlData.output) {
        url += urlData.output;
      }
      return url;
    }
  }),

  retrieveById: linkedinMethod({
    method: 'GET',
    path: function(profileId) {
      return 'id=' + profileId + '/connections';
    }
  }),

  retrieveByUrl: linkedinMethod({
    method: 'GET',
    path: function(url) {
      return 'url=' + urlData.url + '/connections';
    }
  })

});
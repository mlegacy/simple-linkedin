var LinkedinResource = require('../LinkedinResource');
var linkedinMethod = LinkedinResource.method;

module.exports = LinkedinResource.extend({

  path: 'people/~/suggestions/to-follow/companies',

  retrieve: linkedinMethod({
    method: 'GET'
  })

});
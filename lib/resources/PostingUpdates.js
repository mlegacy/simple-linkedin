var LinkedinResource = require('../LinkedinResource');
var linkedinMethod = LinkedinResource.method;

module.exports = LinkedinResource.extend({

  path: 'people/~/person-activities',

  create: linkedinMethod({
    method: 'POST'
  })

});
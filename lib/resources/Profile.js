var LinkedinResource = require('../LinkedinResource');
var linkedinMethod = LinkedinResource.method;

module.exports = LinkedinResource.extend({
  filters: [
    'id', 
    'first-name', 
    'last-name', 
    'formatted-name', 
    'headline', 
    'industry', 
    'num-connections', 
    'picture-url',
    'email-address',
    'interests',
    'skills',
    'positions'
  ],

  path: 'people',

  retrieveOwn: linkedinMethod({
    method: 'GET',
    path: '/~'
  }),

  retrieveById: linkedinMethod({
    method: 'GET',
    path: function(profileId) {
      return '/id=' + profileId;
    }
  }),

  retrieveByUrl: linkedinMethod({
    method: 'GET',
    path: function(url) {
      return '/url=' + url;
    }
  })  

});
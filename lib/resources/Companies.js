var LinkedinResource = require('../LinkedinResource');
var linkedinMethod = LinkedinResource.method;

module.exports = LinkedinResource.extend({

  path: 'companies',

  retrieve: linkedinMethod({
    method: 'GET',
    path: function(companyId) {
      if (companyId) {
        return '/' + companyId;
      }
    }
  }),

  retrieveUniversal: linkedinMethod({
    method: 'GET',
    path: function(universalName) {
      return '/universal-name=' + universalName;
    }
  }),
  
  retrieveEmailDomain: linkedinMethod({
    method: 'GET',
    path: function(emailDomain) {
      return '?email-domain=' + emailDomain;
    }
  }),

  retrieveMix: linkedinMethod({
    method: 'GET',
    path: function (mixFields) {
      return mixFields;
    }
  }),

  retrieveIsAdmin: linkedinMethod({
    method: 'GET',
    path: function(isAdmin) {
      return '?is-company-admin=' + isAdmin;
    }
  })

});
var LinkedinResource = require('../LinkedinResource');
var linkedinMethod = LinkedinResource.method;

module.exports = LinkedinResource.extend({

  path: 'people/~/network/updates',

  retrieve: linkedinMethod({
    method: 'GET',
    path: function (networkKey) {
      return '/key=' + networkKey + '/update-comments';
    }
  }),

  create: linkedinMethod({
    method: 'POST',
    path: function (networkKey) {
      return '/key=' + networkKey + '/update-comments';
    }
  })

});
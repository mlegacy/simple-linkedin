var LinkedinResource = require('../LinkedinResource');
var linkedinMethod = LinkedinResource.method;

module.exports = LinkedinResource.extend({

  path: 'people/~/following/companies',

  retrieve: linkedinMethod({
    method: 'GET'
  }),

  follow: linkedinMethod({
    method: 'POST'
  }),

  unfollow: linkedinMethod({
    method: 'DELETE',
    path: function(companyId) {
      return '/id=' + companyId;
    }
  })

});
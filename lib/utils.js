var _ = require('lodash');
var hasOwn = {}.hasOwnProperty;
var querystring = require('querystring');

var utils = module.exports = {

  isOptionsHash: function(o) {
    return _.isPlainObject(o);
  },

  isObject: function(o) {
    return _.isPlainObject(o);
  },

  getDataFromArgs: function(args) {
    if (args.length > 0) {
      if (utils.isObject(args[0]) && !utils.isOptionsHash(args[0])) {
        return args.shift();
      }
    }
    return {};
  },

  stringifyFilters: function(filters) {
    return ':(' + filters.join(',') + ')';
  },

  replaceResources: function(path, resources) {
    _.keys(resources, function(key) {
      path = path.replace('{' + key + '}', resources[key]);
    });
  },

  addData: function(path, data) {
    return path + '?' + querystring.stringify(data);
  },

  protoExtend: function(sub) {
    var Super = this;
    var Constructor = hasOwn.call(sub, 'constructor') ? sub.constructor : function() {
      Super.apply(this, arguments);
    };
    Constructor.prototype = Object.create(Super.prototype);
    for (var i in sub) {
      if (hasOwn.call(sub, i)) {
        Constructor.prototype[i] = sub[i];
      }
    }
    for (i in Super) {
      if (hasOwn.call(Super, i)) {
        Constructor[i] = Super[i];
      }
    }
    return Constructor;
  }

};
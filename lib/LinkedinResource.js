var utils = require('./utils');
var request = require('request');
var path = require('path');
var Promise = require('bluebird');

LinkedinResource.extend = utils.protoExtend;
LinkedinResource.method = require('./LinkedinMethod');

function LinkedinResource(linkedin, urlData) {
  this._linkedin = linkedin;
  this._urlData = urlData || {};

  this.initialize.apply(this, arguments);
}

LinkedinResource.prototype = {
  path: '',

  filters: [],

  initialize: function() {},

  createDeferred: function(callback) {
      var deferred = Promise.defer();

      if (callback) {
        // Callback, if provided, is a simply translated to Promise'esque:
        // (Ensure callback is called outside of promise stack)
        deferred.promise.then(function(res) {
          setTimeout(function(){ callback(null, res) }, 0);
        }, function(err) {
          setTimeout(function(){ callback(err, null); }, 0);
        });
      }

      return deferred;
  },

  _request: function(method, path, resources, data, callback) {
    var host = this._linkedin.getApi();

    var headers = {
      'Accept': 'application/json',
      'x-li-format': this._linkedin.getFormat(),
      'Authorization': 'Bearer ' + this._linkedin.getAccessToken()
    };

    if (path) {
      path = this.path + path;
    } else {
      path = this.path;
    }

    if (resources) utils.replaceResources(path, resources);

    if (this.filters.length > 0) path += utils.stringifyFilters(this.filters);
    
    if (method !== 'GET') {
      if (typeof(data) === 'string') {
        delete headers['x-li-format'];
      } else {
        headers['Content-type'] = 'application/json';
      }

      request({
        method: method,
        url: host + path,
        json: typeof(data) === 'object' ? true : false,
        body: data,
        headers: headers
      }, callback);
    } else {
      if (data) path = utils.addData(path, data);

      request({
        url: host + path,
        headers: headers
      }, callback);
    }
  }
};

module.exports = LinkedinResource;
var utils = require('./utils');

module.exports = function(spec) {
  var path = spec.path;
  var method = spec.method;

  return function() {
    var self = this;
    var args = [].slice.call(arguments);

    var callback = typeof args[args.length - 1] == 'function' && args.pop();
    var deferred = this.createDeferred(callback);
    var data = utils.getDataFromArgs(args);

    var data = {};
    var resources = {};

    if (args.length == 1) {
      var reqData = args[0];

      if ('resources' in reqData) resources = reqData.resources;
      if ('data' in reqData) data = reqData.data;
    }

    var requestCallback = function(err, response, body) {
      if (err) {
        deferred.reject(err);
      } else {
        deferred.resolve({response: response, body: body});
      }
    };

    this._request(spec.method, spec.path, resources, data, requestCallback);

    return deferred.promise;
  }

};
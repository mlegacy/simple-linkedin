Linkedin.DEFAULT_HOST = 'https://api.linkedin.com';
Linkedin.DEFAULT_BASE_PATH = '/v1/';
Linkedin.DEFAULT_FORMAT = 'json';

var resources = {

  Profile: require('./resources/Profile'),
  Connections: require('./resources/Connections'),
  PeopleSearch: require('./resources/PeopleSearch'),
  Companies: require('./resources/Companies'),
  CompanySearch: require('./resources/CompanySearch'),
  CompanyFollowing: require('./resources/CompanyFollowing'),
  CompanyFollowSuggestions: require('./resources/CompanyFollowSuggestions'),
  CompanyProducts: require('./resources/CompanyProducts'),
  Jobs: require('./resources/Jobs'),
  JobSearch: require('./resources/JobSearch'),
  JobBookmarksSuggestions: require('./resources/JobBookmarksSuggestions'),
  Messaging: require('./resources/Messaging'),
  NetworkUpdates: require('./resources/NetworkUpdates'),
  PostingUpdates: require('./resources/PostingUpdates'),
  PostingShares: require('./resources/PostingShares'),
  Comments: require('./resources/Comments'),
  Likes: require('./resources/Likes')

};

Linkedin.LinkedinResource = require('./LinkedinResource');
Linkedin.resources = resources;

function Linkedin(accessToken) {

  if (!(this instanceof Linkedin)) {
    return new Linkedin(accessToken);
  }

  this.setApiBaseUrl();
  this.setAccessToken(accessToken);
  this.setDefaultFormat(Linkedin.DEFAULT_FORMAT);
  this._prepResources();
}

Linkedin.prototype = {
  setApiBaseUrl: function() {
    this.api = Linkedin.DEFAULT_HOST + Linkedin.DEFAULT_BASE_PATH;
  },

  setAccessToken: function(token) {
    this.accessToken = token;
  },

  setDefaultFormat: function(format) {
    this.format = format;
  },

  getApi: function() {
    return this.api;
  },

  getAccessToken: function() {
    return this.accessToken;
  },

  getFormat: function() {
    return this.format;
  },

  _prepResources: function() {
    for (var name in resources) {
      this[name[0].toLowerCase() + name.substring(1)] = new resources[name](this);
    }
  }
};

module.exports = Linkedin;